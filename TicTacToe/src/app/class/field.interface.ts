//Interfaz para instanciar cada campo del tablero y sus caracteristicas
export interface Field{
    mark: boolean;
    ind_x:boolean,
    ind_o:boolean,
    position: string | null;

}
//Interfaz para instanciar jugadores y definir sus caracteristicas
export interface Player{
    id: string,
    name: string,
    status: string
}
//Interfaz para instanciar un juego y definir sus caracteristicas
export interface Game{
    status: boolean;
    type: string;
    code: string;
    players: Player[];
    ready: boolean;
}
