import { Component } from '@angular/core';
import { Field, Player, Game } from '../class/field.interface'
import { FieldComponent } from '../field/field.component';
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent {
  //Instancia de la interfaz Game
  public game: Game = {status: false, type: "new", code: "12345", players: [{id: "1", name: "Player 1", status: "" },{id: "2", name: "Player 2", status: "" }], ready: true}
  //Instancia de la intefaz Field
  public field: Field = { mark: false, position: null, ind_o: false, ind_x: false };
  //Mensaje que guardara el valor del resultado 
  public message: string = "";
  //Matriz de fields, guarda cada posición del tablero
  public fields: Field[] = [
    { mark: false, position: null, ind_o: false, ind_x: false },//1
    { mark: false, position: null, ind_o: false, ind_x: false },//2
    { mark: false, position: null, ind_o: false, ind_x: false },//3
    { mark: false, position: null, ind_o: false, ind_x: false },//4
    { mark: false, position: null, ind_o: false, ind_x: false },//5
    { mark: false, position: null, ind_o: false, ind_x: false },//6
    { mark: false, position: null, ind_o: false, ind_x: false },//7
    { mark: false, position: null, ind_o: false, ind_x: false },//8
    { mark: false, position: null, ind_o: false, ind_x: false }//9
  ];



  constructor() {
    //La primera vez que carga la pagina comienza un nuevo juego
   this.gameStart();
   //Se guarda en localStorage el primer jugador que comienza la partida
   localStorage.setItem('player_ini', 'X');
  }

//Función para intercalar que jugador comienza la partida luego de ser reiniciada
//Se usan las variables almacenadas en LocalStorage para con una condición definir que jugador inicia
  gameStart(){
    if (localStorage.getItem('int_usuario') == null || localStorage.getItem('int_usuario') == "") {
      localStorage.setItem('int_usuario', 'X');
    } else {
      if (localStorage.getItem('int_usuario') == "X") {
        localStorage.setItem('int_usuario', 'O');
      } else {
        localStorage.setItem('int_usuario', 'X');
      }
    }
  }

//Función que contiene el algoritmo a realizar cada vez que se interactua cun un campo en el tablero
  checkField(field: Field, i: number) {
    //Cuando el juego a finalizado se realiza una condicional que comienza un nuevo juego
    //Tambien se define que jugador iniciara el proximo juego
    if(this.game.ready == false){
       localStorage.setItem('int_usuario', String(localStorage.getItem('player_ini')));
       this.gameStart();
       this.game.ready = true;
    }
    //Cuando un campo ya ha sido marcado no permite asignarle otro valor
    if (field.mark == true) {
      return;
    }
    //Cuando la partida ha finalizado no permite seguir marcando campos vacios
    if(this.game.status == true){
      return;
    }
    //Se realizan condiciones para definir los turnos de cada jugador
    let type_field = localStorage.getItem('int_usuario');
    if (type_field == "X") {
      field = { mark: true, position: String(i), ind_x: true, ind_o: false };
      localStorage.setItem('int_usuario', 'O');
    } else if (type_field == "O") {
      field = { mark: true, position: String(i), ind_o: true, ind_x: false };
      localStorage.setItem('int_usuario', 'X');
    } else {
      field = { mark: false, position: String(i), ind_x: false, ind_o: false };
      localStorage.setItem('int_usuario', "");
    }
    //Se le asigna a la instancia fields cada campo que se haya marcado con sus diferentes atributos
    this.fields[i] = field;
    //Se llama la funcion resultGame() que es la que valida las condiciónes para ganar el juego
    let temp = this.resultGame(String(type_field));
    //Si la función devuelve un valor verdadero, signifca que se termino el juego
    if(temp == true){
      return;
    }
  }

//Función que maneja la logica del juego, se tiene un arreglo con cada condición para ganar el juego
  resultGame(type: string) : string | boolean | undefined {
    let win_condition = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

//Se recorre el arreglo de las condiciones para ganar y se hace una condicional con en arreglo que almacena
//los campos guardados, si el jugador X cumple alguna de las condiciones para ganar se retorna un mensaje
      for (let i = 0; i < win_condition.length; i++) {
        let valid_act = win_condition[i];
        if (this.fields[valid_act[0]].ind_x == true &&
          this.fields[valid_act[1]].ind_x == true &&
          this.fields[valid_act[2]].ind_x == true) {
          this.message = "El jugador X ha ganado";
          this.game.status = true;
          this.game.ready = false;
          return true;
          break;
        }
      }
     
//Si el jugador O cumple alguna de las condiciones para ganar se retorna un mensaje
      for (let i = 0; i < win_condition.length; i++) {
        let valid_act = win_condition[i];
        if (this.fields[valid_act[0]].ind_o == true &&
          this.fields[valid_act[1]].ind_o == true &&
          this.fields[valid_act[2]].ind_o == true) {
          this.message = "El jugador O ha ganado";
          this.game.status = true;
          this.game.ready = false;
          return true;
          break;
        }
      }
      //Se recorre el arreglo que almacena los campos para validar si es un empate
      let cont = 0;
      for (let i = 0; i < this.fields.length; i++) {
        if (this.fields[i].mark == true) {
          cont++;
        }
      }
      //Si su valor equivale a 9, se han llenado todas las casillas sin que gane un jugador
      //Por lo tanto es un empate
      if(cont == 9){
        this.message = "El juego ha terminado en empate";
        this.game.status = true;
        this.game.ready = false;
        return true;
      }
     
    return false
  }

//Fución que resetea los valores de las variables del componente, esta función la activa el boton
//que reinicia la partida, y comienza una nueva
  resetGame(){
    for(let i = 0; i < this.fields.length; i++){
       let current_player = localStorage.getItem('player_ini');
       let ind_actual =  current_player == "O" ? "X" : "O";
       this.fields[i].ind_o = false;
       this.fields[i].ind_x = false;
       this.fields[i].mark = false;
       this.fields[i].position = null;
       this.message = "";
       this.game.ready = false;
       this.game.status = false;
       localStorage.setItem('player_ini', ind_actual);
       this.gameStart();
    }
  }




}