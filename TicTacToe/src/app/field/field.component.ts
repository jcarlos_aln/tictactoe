import { Component, OnInit, Input } from '@angular/core';
import { Field } from '../class/field.interface';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {
  @Input('content') content: Field | undefined; 
  constructor() { }

  ngOnInit(): void {
  }

}
