import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FieldComponent } from '../field/field.component';
import { BoardComponent } from '../board/board.component';
import { IndexGameComponent } from '../index-game/index-game.component';

//Rutas de la aplicación, defino new_game como la principal
//Si se envia una ruta vacia o una ruta que no existe redirige a new_game 

const routes: Routes = [
{
  path: 'new_game',
  component: BoardComponent
},
{
  path: 'index_game' ,
  component: IndexGameComponent
},
{
  path: '',
  pathMatch: 'full',
  redirectTo: 'new_game'
},
{
  path: '**',
  pathMatch: 'full',
  redirectTo: 'new_game'
},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
