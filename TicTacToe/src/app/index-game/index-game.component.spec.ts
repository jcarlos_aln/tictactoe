import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexGameComponent } from './index-game.component';

describe('IndexGameComponent', () => {
  let component: IndexGameComponent;
  let fixture: ComponentFixture<IndexGameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexGameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
